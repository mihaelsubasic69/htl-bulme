---
title: Programming in C
subtitle: Working with Multiple Files
layout: layouts/presentation.njk
---

<section>
  <h2>Programming Projects</h2>

  <ul>
    <li>Growing projects raise the need to use multiple source files</li>
    <li>Functionality belonging together is kept in the same file
      (aka. <em>module</em>)</li>
    <li>Rule of thumb: different concerns go into different files; eg.
      <ul>
        <li>Code for user interaction</li>
        <li>Retrieving data from and storing data in a database</li>
        <li>Business logic and algorithms</li>
      </ul>
    </li>
  </ul>
</section>

<section>
  <h2>Working with Multiple Files</h2>

  <section>
  <h3>Which problems do multiple files solve?</h3>
  <ul class="fragment">
    <li>Sharing code for re-use between programs becomes possible</li>
    <li>Functionality can be logically grouped together into modules</li>
    <li>Multiple programmers can work on a project without frequent
      conflicts
    </li>
    <li>Compile time can be drastically reduced (only re-compile affected
      code)</li>
    <li>Multiple processors / cores can be used for compilation</li>
    <li>Interface code can be clearly separated out into header files</li>
  </ul>
  </section>

  <section>
  <h3>Which New Problems Do They Cause?</h3>
  <ul>
    <li>Larger projects increase complexity</li>
    <li>Splitting functionality requires experience (software architecture)</li>
    <li>Compilation becomes complicated => we need build automation tools</li>
    <li>Build tools need to know about dependencies</li>
    <li>Intermediate files are created and might need to be cleaned up</li>
  </ul>

  </section>
</section>


<section>
  <h2>Compiling and Executing a C Program</h2>

  <section>
    <p>For creating executables, a compiler toolchain is needed.</p>
    <ul>
      <li><a href="http://gcc.gnu.org/">GCC</a> - GNU Compiler Collection</li>
      <li><a href="https://clang.llvm.org/">Clang</a> - C language family
        frontend for LLVM</li>
      <li>MSVC - Microsoft Visual C and C++ compiler</li>
    </ul>
  </section>

  <section>
    <p>
      Creating a C executable is a four step process.
    </p>
    <ol>
      <li>Preprocessor</li>
      <li>Compiler</li>
      <li>Assembler</li>
      <li>Linker</li>
    </ol>
  </section>

  <section>
    <h3>Example</h3>
      <p>Download <a href="/data/c/hello.c">hello.c</a></p>
      <p>By default, all three stages (preprocessor, compiler and linker) are done</p>
{% highlight "bash" %}
clang hello.c
./a.out
{% endhighlight %}
<div class="fragment">
<p>Provide a name for the created executable (instead of the default
  <code>a.out</code>
</p>
{% highlight "bash" %}
clang hello.c -o hello
./hello
{% endhighlight %}
</div>
  </section>

  <section>
    <h3>1. Preprocessor</h3>
    <ul>
      <li>Essentially a pure text processor</li>
      <li>Removes comments, performs includes etc.</li>
      <li>Usually never done separately</li>
    </ul>
    <div class="fragment">
      <p>Stop after preprocessor and send output to <code>stdout</code></p>
{% highlight "bash" %}
clang -E hello.c
{% endhighlight %}
    </div>
  </section>

  <section>
    <h3>2. Compiler</h3>
    <ul>
      <li>Translates high level code to a lower level (assembly language)</li>
      <li>Parses the source code</li>
      <li>Performs type checking</li>
      <li>Usually never done separately</li>
      <li>Must recognize all identifiers (function declarations etc.)</li>
    </ul>
    <div class="fragment">
      <p>Creates assembler files (usually <code>*.s</code>)</p>
{% highlight "bash" %}
clang -S hello.c
{% endhighlight %}
    </div>
  </section>

  <section>
    <h3>3. Assembler</h3>
    <ul>
      <li>Translates assembly language to object code (machine language)</li>
      <li>Creates <code>*.o</code> files</li>
      <li><strong>Usually done separately</strong> when working with multiple input files
        <ul class="fragment">
          <li>Impossible to create executables without <code>main(.)</code></li>
          <li>Impossible to create executables with multiple <code>main(.)</code></li>
        </ul>
      </li>
    </ul>
    <div class="fragment">
{% highlight "bash" %}
clang -c hello.c
{% endhighlight %}
    </div>
  </section>

  <section>
    <h3>4. Linker</h3>
    <ul>
      <li>Combines object files into a unified executable program, resolving all symbols</li>
      <li><strong>Has to be run separately</strong> when working with multiple input files</li>
    </ul>
    <div class="fragment">
{% highlight "bash" %}
clang -o hello hello.o
{% endhighlight %}
    </div>
  </section>
</section>

<section>
  <h2>Header Files</h2>
  <section>
    <h3>Why do we Need Header Files?</h3>
    <p>Imagine a project with three files: <code>main.c</code> and modules
      <code>array.c</code> and <code>ui.c</code></p>
    <div>
      {% for filename in ['main.c', 'array.c', 'ui.c'] %}
      <div class="inline-box" style="width: 120px; height: 150px;">
        <img src="/images/icons/text-x-csrc.svg" alt="[csource icon]"><br>
        <code>{{ filename }}</code>
      </div>
      {% endfor %}
      <div class="inline-box"
           style="vertical-align: middle; height: 150px;">=></div>
      <div class="inline-box" style="width: 120px; height: 150px;">
        <img src="/images/icons/application-x-executable.svg"
             alt="[csource icon]"><br>
        <code>executable</code>
      </div>
    </div>
    <p>What do we have to do to satisfy the compiler when using functionality
      of <code>array.c</code> in all other files?</p>
    <p class="fragment">What if we have thousands of files and change
      <code>array.c</code>?</p>
  </section>

  <section>
    <h3>Solution</h3>
    <p>Write the declarations exported by a source file into a separate header
      file.
    </p>
    <p class="fragment">Include the header file where the declarations are used.</p>
    <p class="fragment">
      <strong>Header files do not need to be compiled separately</strong>
      since their content is copied into the source files by the preprocessor.
    </p>
    <div class="fragment">
      <p>Example</p>
      <ol>
        <li>main.c uses functions defined in array.c and ui.c</li>
        <li>ui.c uses functions defined in array.c</li>
      </ol>
    </div>
  </section>

  <section>
    <img src="/images/programming/c_header_files.png" alt="project overview">
  </section>

  <section>
    <h3><code>extern</code> and <code>static</code> Functions in C</h3>
      <p>Every identifier that is marked as <code>extern</code> is part
        of a file's public interface.
        These identifiers can be used in other files by including the
        header file.</p>
      <ul>
        <li>Functions are automatically <code>extern</code></li>
        <li>Use the <code>extern</code> keyword to share variables (if really,
      really needed)</li>
        <li>In C, the <code>static</code> keyword marks functions that should
          not be exported</li>
        <li class="fragment"><code>static</code> C functions cannot be used in
          other files of the same project</li>
      </ul>
  </section>

  <section>
    <h3><code>#include</code> Guards</h3>
    <p>A construct used to avoid the problem of double inclusion when dealing
      with the include directive.</p>
{% highlight "c" %}
#ifndef FILENAME_H
#define FILENAME_H

// ... your header file's code

#endif // FILENAME_H
{% endhighlight %}
    <p class="fragment"><strong><code>#include</code> guards</strong>
      can also be referred to as
      <strong>macro guards</strong>, <strong>header guards</strong>
      or <strong>file guards</strong></p>
  </section>

</section>

<section>
  <h2>Compiling Multiple Files</h2>
  <section>
    <p>Compiling all source files produces a single executable.<br />
      Remember: <code>*.h</code> files are already included in the
      <code>*.c</code> files.</p>
{% highlight "bash" %}
clang *.c
{% endhighlight %}
    <p class="fragment">It would be much faster to re-compile
      only the files that need to be re-compiled and then link the
      object files.<br />
      How do we know what needs re-compilation?
    </p>
    <p class="fragment">
      Everything affected by a change must be re-compiled. If, for example,
      a header file is changed, we need to re-compile all <code>*.c</code>
      files that <code>#include</code> that header file.
    </p>
  </section>

  <section>
    <h3>Example 1/2</h3>
    <p>Download <a href="/data/c/multiple_files/main.c">main.c</a>,
      <a href="/data/c/multiple_files/ui.c">ui.c</a>,
      <a href="/data/c/multiple_files/ui.h">ui.h</a>,
      <a href="/data/c/multiple_files/array.c">array.c</a> and
      <a href="/data/c/multiple_files/array.h">array.h</a>
      (or {% archive 'data/c/multiple_files/' %}).<br />
      Compile all files at once:
    </p>
{% highlight "bash" %}
clang *.c -o array_tool
{% endhighlight %}
    <div class="fragment">

    <p>Since we did not create object files, any change would require
      complete re-compilation. Instead, we should create object files and
      link them together.
    </p>
{% highlight "bash" %}
clang -c *.c
clang *.o -o array_tool
{% endhighlight %}
    </div>
  </section>

  <section>
    <h3>Example 2/2</h3>
    <p>Now, make a change to <code>main.c</code>. Check the return value
    of <code>get_dimension(.)</code> and issue a warning if the value is 0.</p>
    <p>Once done, we can just re-compile <code>main.c</code> and link to the
    existing object files.</p>
{% highlight "bash" %}
clang -c main.c
clang *.o -o array_tool
{% endhighlight %}
  </section>
</section>

<section>
  <h2>Exercise</h2>
  <p>Extend the program to contain another function<br />
    <code>int amax(int *array, size_t dimension)</code><br />
    that returns the
    maximum of the given array. Add the function declaration to
    <code>array.h</code> and the definition to <code>array.c</code>.
    Call the function in <code>main.c</code> and print the result to
    <code>stdout</code>.
  </p>
  <p>Which files need to re-compiled to object files before a new executable
    can be linked? Once you know the answer, compile them and create a new
    executable.
  </p>
</section>

<section>
  <h2>Build Automation Tools</h2>
  <section>
  <p>Build tools allow you to re-compile exactly the files that need to
    be re-compiled with a single command or the click of a button.
  </p>
  <p>There are many different build tools for each programming language.<br />
    For C and C++, popular examples are
  </p>
  <ul>
    <li>Visual Studio (also takes care of this)</li>
    <li>GNU make</li>
    <li>CMake</li>
    <li>...</li>
  </ul>
  </section>

  <section>
    <h3>GNU Make</h3>
      <ul>
        <li>Controls the generation of executables and other non-source
          files</li>
        <li>+ Independent of IDE</li>
        <li>+ Open source</li>
        <li>+ Allows compiling files in parallel</li>
        <li>- Platform dependent</li>
        <li>- Requires a complicated <code>Makefile</code> for the
          project</li>
      </ul>
      <p><a href="https://www.gnu.org/software/make/manual/html_node/Quick-Reference.html">GNU make quick reference</a></p>
  </section>

  <section>
    <h3>GNU Make Example 1/2</h3>
{% highlight "makefile" %}
{% include "source/c/multiple_files/Makefile" %}
{% endhighlight %}
    <p>{% download "/data/c/multiple_files/Makefile" %}</p>
    <p class="fragment">Makefiles quickly get complicated:
      <a href="https://github.com/senarclens/cvrptwms/blob/master/Makefile">
        example Makefile using more features</a></p>
  </section>

  <section>
    <h3>GNU Make Example 2/2</h3>
    <p>To make a build, run <code>make -f YourMakeFile</code></p>
    <p class="fragment">
      If no <kbd>-f</kbd> option is present, make will look for the makefiles
      <code>GNUmakefile</code>, <code>makefile</code>, and
      <code>Makefile</code>, in that order.</p>
    <div class="fragment">
{% highlight "bash" %}
make  # run the default (first) target
make array_tool  # run the target explicitly (usually it is aliased to `all`)
make clean  # run the target clean
make array.o  # run the target to build array.o if that is needed
make -j8  # run 8 jobs in parallel (use up to 8 CPU cores)
{% endhighlight %}
    </div>
  </section>

  <section>
    <h3>CMake</h3>
      <ul>
        <li>+ Powerful, cross-platform build environment</li>
        <li>+ Independent of IDE</li>
        <li>+ Open source</li>
        <li>+ Harnesses the power of GNU Make by generating the
          <code>Makefile</code>
        </li>
        <li>+ Builds "out of source" in a separate directory by default</li>
      </ul>
      <p><a href="https://cmake.org/cmake/help/latest/">CMake Documentation</a></p>
  </section>

  <section>
    <h3>CMake Example 1/2</h3>
    {# add cmake file (source and link) + usage instructions #}
{% highlight "cmake" %}
{% include "source/c/multiple_files/CMakeLists.txt" %}
{% endhighlight %}
    <p>{% download "/data/c/multiple_files/CMakeLists.txt" %}</p>
    <p class="fragment">CMake isn't trivial either:
      <a href="https://github.com/senarclens/cvrptwms/blob/master/CMakeLists.txt">
        example CMakeLists.txt using more features</a></p>
  </section>

  <section>
    <h3>CMake Example 2/2</h3>
    <p></p>
    <p>To make a build, run the following commands</p>
{% highlight "bash" %}
mkdir build  # create directory for all files created during build
cd build
cmake ..  # create the Makefile for your setup
make -j8  # start actual build on 8 cores; alternatively `cmake --build . -j8`
{% endhighlight %}
  </section>
</section>

{% include 'slides/closing.njk' %}
