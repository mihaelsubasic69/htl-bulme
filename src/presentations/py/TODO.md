microservice architecture w/ python

1. freeCodeCamp just published a hands-on Microservice Architecture course. This is a great way to learn about Distributed Systems. You can code along at home, and build your own video-to-MP3 file converter app. Along the way, you'll learn some MongoDB, Kubernetes, and MySQL. (5 hour YouTube course): https://www.freecodecamp.org/news/microservices-and-software-system-design-course/
