---
title: Programming in C++
subtitle: Standard Library 1 - Containers
layout: layouts/presentation.njk
---

<section>
  <h2>C++ Standard Library</h2>

  <section>
  <ul>
    <li>Part of the C++ ISO Standard</li>
    <li>Heavily influenced by the Standard Template Library (STL)</li>
    <li>Provides
      <ul>
        <li>Generic <strong>container data types</strong></li>
        <li>Streams</li>
        <li>Generic algorithms (syntax, semantics and performance)</li>
        <li>Regular expressions, filesystem interaction, concurrency, ...</li>
      </ul>
    </li>
    <li><a href="https://en.cppreference.com/w/">C++ reference</a></li>
  </ul>
  </section>

  <section>
    <h3>Advantages</h3>
    <ul>
      <li>Allows focusing on the project at hand</li>
      <li>Fulfills documented performance requirements</li>
      <li>Available with every common compiler:
        <a href="https://en.cppreference.com/w/cpp/compiler_support">compiler support</a></li>
      <li>Continues to grow with every version of C++</li>
    </ul>
  </section>
</section>

<section>
  <h2>Container Data Types</h2>
  <p>The standard library provides many container data types</p>
  <div class="fragment">
    <p>The most important container types are</p>
    <ul>
      <li><code>vector</code> (+ <code>array</code>) (contiguous memory)</li>
      <li><code>list</code> (doubly linked list)</li>
      <li><code>map</code> (hashmap)</li>
      <li><code>set</code></li>
    </ul>
  </div>
  <p class="fragment">Overview of <a href="https://en.cppreference.com/w/cpp/container">all
    container types</a></p>
</section>

<section>
  <h2>Array</h2>
  <section>
    <p>
      <a href="https://en.cppreference.com/w/cpp/container/array">
        <code>std::array</code></a>
      is a container that encapsulates fixed size arrays</p>
{% highlight "cpp" %}
template<class T, std::size_t N> struct array;
{% endhighlight %}
    <div class="fragment">
      <p>For example</p>
{% highlight "cpp" %}
std::array<int, 3> a1{1, 2, 3};
std::array a2{1, 2, 3};  // requires C++17 or newer
{% endhighlight %}
    </div>
  </section>

  <section>
    <h3>Example: Standard Operations</h3>
{% highlight "cpp" %}
{% include "source/cpp/standard_library/array.cpp" %}
{% endhighlight %}
{% include_code "source/cpp/standard_library/array.cpp" %}
  </section>
</section>

<section>
  <h2>Vector</h2>

  <section>
    <p>
      <a href="https://en.cppreference.com/w/cpp/container/vector">
        <code>std::vector</code></a>
      is a dynamic contiguous array - grows "automatically"</p>
    <ul>
      <li>Random access: constant $O(1)$</li>
      <li>Insertion or removal at the end: amortized constant $O(1)$</li>
      <li>Insertion or removal: linear in the distance to the end $O(n)$</li>
    </ul>
    <p>More powerful and easier to use than <code>array</code></p>
{% highlight "cpp" %}
template<class T, class Allocator = std::allocator<T>> class vector;
{% endhighlight %}
    <div class="fragment">
      <p>For example</p>
{% highlight "cpp" %}
std::vector<int> v{3, 2, 1};
{% endhighlight %}
    </div>
  </section>

  <section>
    <h3>Example: Pass Vector to Function</h3>
{% highlight "cpp" %}
{% include "source/cpp/standard_library/vector.cpp" %}
{% endhighlight %}
{% include_code "source/cpp/standard_library/vector.cpp" %}
  </section>
</section>

<section>
  <h2>Doubly Linked List</h2>

  <section>
    <p>
      <a href="https://en.cppreference.com/w/cpp/container/list">
        <code>std::list</code></a>
      elements have a pointer to next and previous element</p>
    <ul>
      <li>Random access: linear in the distance from head / tail $O(n)$</li>
      <li>Insertion or removal: constant $O(1)$</li>
    </ul>
    <p>Memory is not contiguous</p>
{% highlight "cpp" %}
template<class T, class Allocator = std::allocator<T>> class list;
{% endhighlight %}
    <div class="fragment">
      <p>For example</p>
{% highlight "cpp" %}
std::list<int> l{12, 99, 37};
{% endhighlight %}
    </div>
  </section>

  <section
    data-background="https://upload.wikimedia.org/wikipedia/commons/5/5e/Doubly-linked-list.svg"
    data-background-size="70%">
  </section>

  <section>
    <h3>Example: Fast Insertion to Middle of List</h3>
{% highlight "cpp" %}
{% include "source/cpp/standard_library/list.cpp" %}
{% endhighlight %}
{% include_code "source/cpp/standard_library/list.cpp" %}
  </section>
</section>

<section>

  <section>
    <h2>Map</h2>
    <p><a href="https://en.cppreference.com/w/cpp/container/map">
      <code>std::map</code></a>
      is a collection of key-value pairs with unique keys</p>
    <ul>
      <li>Search, removal, and insertion: logarithmic $O(\log{n})$</li>
    </ul>
    <p>Keys are sorted and maps are usually implemented as
      <a href="https://en.wikipedia.org/wiki/Red%E2%80%93black_tree">red-black
        trees</a></p>
{% highlight "cpp" %}
template<class Key, class T, class Compare = std::less<Key>,
         class Allocator = std::allocator<std::pair<const Key, T>>> class map;
{% endhighlight %}
    <div class="fragment">
      <p>For example</p>
{% highlight "cpp" %}
std::map<std::string, std::string> m{ {"key1": "value1"}, {"key2": "value2"} };
{% endhighlight %}
    </div>
  </section>

  <section
    data-background="https://upload.wikimedia.org/wikipedia/commons/d/d0/Hash_table_5_0_1_1_1_1_1_LL.svg"
    data-background-size="contain">
  </section>

  <section>
    <h2>Map</h2>
    <h3>Example: Working with Maps</h3>
{% highlight "cpp 6" %}
{% include "source/cpp/standard_library/map.cpp" %}
{% endhighlight %}
{# unfortunately too complicated due to limitations of pythontutor.com
   => using download option instead
{% include_code "source/cpp/standard_library/map.cpp" %}
#}
{% download "/data/cpp/standard_library/map.cpp" %}
  </section>
</section>

<section>
  <h2>Set</h2>

  <section>
    <p><a href="https://en.cppreference.com/w/cpp/container/set">
      <code>std::set</code></a>
      is a (sorted) collection of unique elements</p>
    <ul>
      <li>Search, removal, and insertion: logarithmic $O(\log{n})$</li>
    </ul>
    <p><code>std::set</code> is useful wherever mathematical sets are useful</p>
{% highlight "cpp" %}
template<class Key, class Compare = std::less<Key>,
         class Allocator = std::allocator<Key>> class set;
{% endhighlight %}
    <div class="fragment">
      <p>For example</p>
{% highlight "cpp" %}
std::set<int> s {1, 2, 3};
{% endhighlight %}
    </div>
  </section>

  <section>
    <h3>Theory - Intersection</h3>
    <p><strong>Intersection</strong> of two sets $A$ and $B$,
      denoted by $A \cap B$ is the set containing all elements of $A$
      that also belong to $B$</p>
      <img src="https://upload.wikimedia.org/wikipedia/commons/9/99/Venn0001.svg"
           alt="[set intersection diagram]">
  </section>

  <section>
    <h3>Theory - Union</h3>
    <p><strong>Union</strong> (denoted by $\cup$) of a collection of sets
        is the set of all elements in the collection</p>
      <img src="https://upload.wikimedia.org/wikipedia/commons/3/30/Venn0111.svg"
           alt="[set union diagram]">
  </section>

  <section>
    <h3>Example: Working with Sets</h3>
{% highlight "cpp 11,18,23" %}
{% include "source/cpp/standard_library/set.cpp" %}
{% endhighlight %}
{# unfortunately std::ranges is not available on pythontutor yet
   => using download option instead
{% include_code "source/cpp/standard_library/set.cpp" %}
#}
{% download "/data/cpp/standard_library/set.cpp" %}
  </section>
</section>

<section>
  <h2>Copy Assignment</h2>
  <section>
  <p>Standard library containers implement the copy constructor</p>
{% highlight "cpp 8,9" %}
{% include "source/cpp/standard_library/copy_assignment.cpp" %}
{% endhighlight %}
{# unfortunately std::ranges is not available on pythontutor yet
   => using download option instead
{% include_code "source/cpp/standard_library/copy_assignment.cpp" %}
#}
{% download "/data/cpp/standard_library/copy_assignment.cpp" %}
  </section>

  <section>
{% highlight "cpp 1,2" %}
std::vector<int> v1(5);  // create vector {0, 0, 0, 0, 0}
std::vector<int> v2 = v1;  // this is copy assignment, not a reference
std::vector<int>& v3 = v1;  // this is a reference
v1[1] = 7;
v2[3] = 2;
v3[2] = 4;
{% endhighlight %}

  <div class="fragment">
    <p>
<pre>
v1: 0 7 4 0 0
v2: 0 0 0 2 0
v3: 0 7 4 0 0
</pre>
    </p>
  </div>

  </section>
</section>

{% include 'slides/closing.njk' %}
