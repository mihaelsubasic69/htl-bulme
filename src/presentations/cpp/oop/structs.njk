---
title: Programming in C++
subtitle: Introduction to Object Oriented Programming (OOP)
subsubtitle: Structs (Composite Data Types)
layout: layouts/presentation.njk
requires: c/structs
---

<section>
  <section>
    <h2>What are C++ Structs</h2>
    <p>C++ structs are, in terms of functionality, a superset of
      <a href="/presentations/c/structs/">C structs</a></p>

    <p class="fragment">C++ structs allow to store multiple variables
      (<strong>data members</strong>) along with behavior
      (<strong>member functions</strong>)
    </p>

    <p class="fragment">C++ structs allow <strong>encapsulation</strong> and
      <strong>inheritance</strong></p>

    <div class="fragment">
    <p>The only difference between <code>struct</code> and
      <code>class</code> in C++ are
      <ul>
        <li><em>access specifier defaults to <code>public</code> for
          <code>struct</code></em>
          and <code>private</code> for <code>class</code></li>
        <li><em>inheritance defaults to <code>public</code> for
          <code>struct</code></em>
          and <code>private</code> for <code>class</code></li>
      </ul>
    </p>
    </div>
  </section>

  <section>
    <h2>Advantages of C++ Structs</h2>
    <p>C++ <code>struct</code>s allow real OOP including advanced concepts
      such as
      <strong>encapsulation</strong> and <strong>inheritance</strong>
    </p>
  </section>

  <section>
    <h2>Disadvantages of C++ Structs</h2>
    <p>In C++ <code>struct</code> and <code>class</code> are (besides their
      default access specifier) identical which can lead to confusion</p>
    <p>It is recommended to use <code>struct</code> in C++ similar to the
      much more limited <code>struct</code> in C
    </p>

    <div class="fragment">
      <p>
        <a href="https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#S-class">
        C++ Core Guidelines</a></p>
        <ul>
          <li>Use struct if the data members can vary independently</li>
        </ul>
    </div>
    <div class="fragment">
      <p>
        <a href="https://google.github.io/styleguide/cppguide.html#Structs_vs._Classes">
        Google C++ Style Guide</a></p>
        <ul>
          <li>Use a struct only for passive objects that carry data</li>
          <li>The struct must not have invariants that imply relationships
            between different fields</li>
        </ul>
    </div>
  </section>
</section>

<section>
  <h2>Defining a C++ <code>struct</code></h2>

  <section>
{% highlight "cpp" %}
struct StructName {
  StructName();  // constructors are supported
  ~StructName();  // destructors are supported

  type member1;
  type member2;
  /* declare as many members as desired */

  type method1(param1, ...);
  type method2(.);
  /* declare as many methods as desired */
};
{% endhighlight %}
  </section>

  <section>
    <h3>Example: <code>struct</code> Point</h3>
{% highlight "cpp 1-4,6,8" %}
{% include "source/cpp/oop/draw/intro.cpp" %}
{% endhighlight %}
{% include_code "source/cpp/oop/draw/intro.cpp" %}
  </section>

  <section>
    <p>A C++ <code>struct</code> can have methods</p>
    <p>Data fields are directly available</p>
    <div class="fragment">
      <h3>Example: Member Functions</h3>
{% highlight "cpp 4-6,10" %}
{% include "source/cpp/oop/draw/method.cpp" %}
{% endhighlight %}
{% include_code "source/cpp/oop/draw/method.cpp" %}
    </div>
  </section>

  <section>
    <p>Member access can be explicit using the <code>this</code> pointer</p>
    <div class="fragment">
      <h3>Example: <code>this</code></h3>
{% highlight "cpp 4-6,10" %}
{% include "source/cpp/oop/draw/this.cpp" %}
{% endhighlight %}
{% include_code "source/cpp/oop/draw/this.cpp" %}
    </div>
  </section>

</section>


<section>
  <h2>Constructor and Destructor</h2>

  <section>
  <p>In OOP, a <strong>constructor</strong> (abbreviation: ctor) is a special
    type of method called to create an object</p>
  <p>A <strong>destructor</strong> (sometimes abbreviated dtor) is a method
    which is invoked just before the memory of the object is released</p>
  </section>

  <section>
    <h3>Example: Constructor for Point</h3>
{% highlight "cpp 4-5,11-12" %}
{% include "source/cpp/oop/draw/constructor.cpp" %}
{% endhighlight %}
{% include_code "source/cpp/oop/draw/constructor.cpp" %}
  </section>
</section>

<section>
  <h2>Free Store</h2>
  <p>In C++, the free store can be used with the <code>new</code> and
    <code>delete</code> keywords.</p>
{% highlight "cpp 11-13" %}
{% include "source/cpp/oop/draw/free_store.cpp" %}
{% endhighlight %}
{% include_code "source/cpp/oop/draw/free_store.cpp" %}
</section>

<section>
  <h2>Separate Declaration and Definition</h2>

  <section>
    <p>To use a <code>struct</code> in other files, declare it in a header file</p>
    <p>Structs are usually also defined in header files</p>
    <p>Struct methods are declared in the struct definition but
      defined in a designated source file</p>
    <div class="fragment">
      <p>
      <a href="https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#Rc-interface">
      C++ Core Guidelines</a></p>
      <ul>
        <li>Explicit distinction between interface (<kbd>*.hpp</kbd>) and
          implementation (<kbd>*.cpp</kbd>) improves
          readability and simplifies maintenance</li>
      </ul>
    </div>
  </section>

  <section>
    <h3>Example: Header File Contains the API</h3>
{% highlight "cpp 7,9" %}
{% include "source/cpp/oop/draw/point.hpp" %}
{% endhighlight %}
{% download "/data/cpp/oop/draw/point.hpp" %}
  </section>

  <section>
    <h3>Example: Source File</h3>
{% highlight "cpp 4,6" %}
{% include "source/cpp/oop/draw/point.cpp" %}
{% endhighlight %}
{% download "/data/cpp/oop/draw/point.cpp" %}
  </section>

  <section>
    <h3>Example: Use the <code>struct</code></h3>
    <p>The <code>struct</code> can be used from any other file</p>
{% highlight "cpp 3,5" %}
{% include "source/cpp/oop/draw/main.cpp" %}
{% endhighlight %}
{% download "/data/cpp/oop/draw/main.cpp" %}
    <p>Compile the program with <kbd>clang++ main.cpp point.cpp</kbd></p>
  </section>
</section>

{# left out on purpose - too much information and shows a "bad example"
<section>
  <h2>Access Specifiers</h2>

  <section>
  In C++, there are three access specifiers: public - members are accessible from outside the class. private - members cannot be accessed (or viewed) from outside the class. protected - members cannot be accessed from outside the class, however, they can be accessed in inherited classes.

  can, but generally shouldn't be done
  https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#c2-use-class-if-the-class-has-an-invariant-use-struct-if-the-data-members-can-vary-independently
  </section>

  <section>
    <h3>Example</h3>
{% highlight "c 4-6" %}
{% include "source/cpp/oop/draw/private.cpp" %}
{% endhighlight %}
{% include_code "source/cpp/oop/draw/private.cpp" %}
  </section>

</section>
#}

<section>
  <h2>Key Differences from C</h2>
  <p>In C++, <code>struct</code> is much more powerful than in C</p>

  <ul>
    <li class="fragment">C++ <code>struct</code> can have member functions (methods)</li>
    <li class="fragment">C++ <code>struct</code> can have constructors and destructors</li>
    <li class="fragment"><code>this</code> keyword is a reference to the
      current instance<br />
      <code>this</code> is implicitly passed to every method</li>
    <li class="fragment"><code>struct</code> keyword not needed when creating
      an instance<br />
      &rArr; no need to <code>typedef</code> a simple <code>struct</code></li>
    <li class="fragment">C++11 allows default values for struct properties</li>
  </ul>

  <p class="fragment">C++ structs are even more powerful, but it is best
    practice to resort to use classes instead when those features are needed
  </p>

</section>


{% include 'slides/closing.njk' %}
