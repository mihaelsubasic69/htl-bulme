---
title: Programming in C++
subtitle: Introduction to Object Oriented Programming (OOP)
subsubtitle: Classes and Encapsulation
layout: layouts/presentation.njk

recommended: structs

CoreCpp: https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#
GoogleCpp: https://google.github.io/styleguide/cppguide.html#
---

<section>

  <section>
    <h2>Problems of a Purely Procedural Approach</h2>
    <p>Using most container types, it isn't obvious what the elements
        represent</p>
{% highlight "cpp" %}
std::vector<double> circle{25, 15, 7};  // what is the radius?
{% endhighlight %}
    <div class="fragment">
      <p>Using a <code>map</code>, we cannot mix data types</p>
{% highlight "cpp" %}
std::map<std::string, std::string> student {
  {"name", "Pat"},
  {"age", "23"},  // age is represented as string
};
{% endhighlight %}
    </div>
    <div class="fragment">
      <p>If data integrity is not guaranteed, nasty bugs happen</p>
{% highlight "cpp" %}
struct Circle { double x, y, radius; };
Circle c { .x = 25, .y = 15, .radius = -7};  // invalid radius
{% endhighlight %}
    </div>
    <p class="fragment">The inability to guarantee the validity of an object is
        likely the worst downside of purely procedural approaches</p>
  </section>

  <section>
  <h2>Advantages of Object Oriented Programming (OOP)</h2>
  <p>In the real world, there is an infinite variation of data types<br />
    <strong>OOP enables you to model your own datatypes</strong></p>

  <ul>
    <li class="fragment">Data consistency (validity) can be ensured via
      <strong>encapsulation</strong></li>
    <li class="fragment">We usually think in terms of <strong>classes</strong>
      and concrete <strong>objects</strong><br />
      &rArr; map the objects in the problem domain to those in the program
    </li>
    <li class="fragment">Problems can be broken into smaller
      parts that can be solved easier</li>
    <li class="fragment">Classes are self-contained code units that facilitate
      maintenance
    </li>
    <li class="fragment">New ways of <strong>code reuse</strong> through
      advanced OOP techniques</li>{# aggregation, composition, inheritance #}
    <li class="fragment">OOP permits the use of <strong>design patterns</strong>
      for larger software systems</li>
  </ul>
  </section>

</section>

<section>
  <h2>Terms</h2>
  <dl>
    <dt><code>class</code></dt>
    <dd>User defined data type</dd>
    <dd>Provides data members/ fields (may be hidden)</dd>
    <dd>Behavior is defined by the class' methods</dd>
    <div class="fragment">
      <dt>Instance / object</dt>
      <dd>Variables with the type of a class are called objects or
        instances</dd>
      <dd>Classes can be thought of as object factories</dd>
    </div>
    <div class="fragment">
      <dt>Method</dt>
      <dd>Function (or procedure) associated with a class</dd>
      <dd>Public methods constitute the class' interface (API)</dd>
    </div>
    <div class="fragment">
      <dt>Class invariant</dt>
      <dd>The invariant constrains the state stored in the object</dd>
      <dd>Methods of the class have to preserve the invariant</dd>
    </div>
  </dl>
</section>

<section>
  <h2>Defining a C++ <code>class</code></h2>

  <section>
{% highlight "cpp" %}
class ClassName {
public:  // private is the default for classes (public for structs)
  ClassName(.);  // constructors are supported
  ~ClassName(.);  // destructors are supported
  type member1;
  type member2;  // declare as many properties as needed
  type method1(param1, ...);
  type method2(.);  // declare as many methods as needed

private:  // these are only accessible within the class
  type priv_member_1_;
  type priv_member_2_;  // declare as many private properties as desired
  type priv_method1(param1, ...);
  type priv_method2(.);  // declare as many private methods as desired

};
{% endhighlight %}
  </section>

  <section>
  <h3>Related Conventions</h3>
  <p>Be consitent
    (eg. <a href="{{ CoreCpp }}S-class">C++ Core Guidelines</a>
    or <a href="{{ GoogleCpp }}Classes">Google C++ Style Guide</a>)</p>
  <p>Always explain the reasons for your decisions</p>

  <dl class="fragment">
    <dt>
      <a href="{{ CoreCpp }}nl16-use-a-conventional-class-member-declaration-order">NL.16</a>
      (<a href="{{ CoreCpp }}S-naming">NL: Naming and layout suggestions</a>)
    </dt>
    <dd>Use the <code>public</code> before <code>protected</code>
      before <code>private</code> order</dd>
    <dt><a href="{{ GoogleCpp }}Variable_Names">Variable Names</a></dt>
    <dd>Private data members of classes with a trailing underscore</dd>
    <dt><a href="{{ CoreCpp }}c48-prefer-in-class-initializers-to-member-initializers-in-constructors-for-constant-initializers">C.48</a>,
      <a href="{{ CoreCpp }}c49-prefer-initialization-to-assignment-in-constructors">C.49</a></dt>
    <dd>Prefer in-class initializers to member initializers in constructors for
      constant initializers</dd>
    <dd>Prefer initialization to assignment in constructors</dd>
  </dl>
  </section>

</section>

<section>
  <h2>Encapsulation</h2>
  <section>
    <p>Used to hide the state of a structured object inside a class</p>
    <p>Prevents direct access to the data members by clients</p>
    <p>Ensures that clients cannot violate the class invariant</p>
    <div class="fragment">
      <p>The invariant can be tested</p>
      <ul>
        <li>explicitly with assertions; for example<br />
          <a href="https://www.geeksforgeeks.org/what-is-class-invariant/">
          https://www.geeksforgeeks.org/what-is-class-invariant/</a></li>
        <li>with unit tests</li>
      </ul>
    </div>
    <dl class="fragment">
      <dt><a href="{{ GoogleCpp }}Access_Control">Access Control</a>,
        <a href="{{ CoreCpp }}c133-avoid-protected-data">C.133</a>,
        <a href="{{ CoreCpp }}c134-ensure-all-non-const-data-members-have-the-same-access-level">C.134</a>
      </dt>
      <dd>Make classes' data members private, unless they are constants.</dd>
    </dl>
  </section>

  <section>
    <h3>Example: <code>class</code> Circle</h3>
{% highlight "cpp 4-6,10-13,17" %}
{% include "source/cpp/oop/circle.cpp" %}
{% endhighlight %}
{% include_code "source/cpp/oop/circle.cpp" %}
  </section>
</section>

<section>
  <h2>Separate Declaration and Definition</h2>
  <section>
    <p>To use a <code>class</code> in other files, declare it in a header
      file</p>
    <p>Classes are usually also defined in header files</p>
    <p>Class methods are declared in the class definition but
      defined in a designated source file</p>
    <div class="fragment">
      <p>
      <a href="https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#Rc-interface">
      C++ Core Guidelines</a></p>
      <ul>
        <li>Explicit distinction between interface (<kbd>*.hpp</kbd>) and
          implementation (<kbd>*.cpp</kbd>) improves
          readability and simplifies maintenance</li>
      </ul>
    </div>
  </section>

  <section>
    <h3>Example: Header File Contains the API</h3>
{% highlight "cpp 7,11-13" %}
{% include "source/cpp/oop/ds/int_vector.hpp" %}
{% endhighlight %}
{% download "/data/cpp/oop/ds/int_vector.hpp" %}
  </section>

  <section>
    <h3>Example: Source File</h3>
{% highlight "cpp 6,15,25" %}
{% include "source/cpp/oop/ds/int_vector.cpp" %}
{% endhighlight %}
{% download "/data/cpp/oop/ds/int_vector.cpp" %}
  </section>

  <section>
    <h3>Example: Use the <code>class</code></h3>
    <p>The <code>class</code> can be used from any other file</p>
{% highlight "cpp 0,4" %}
{% include "source/cpp/oop/ds/int_vector_main.cpp" %}
{% endhighlight %}
{% download "/data/cpp/oop/ds/int_vector_main.cpp" %}
    <p>Compile the program with
      <kbd>clang++ int_vector_main.cpp int_vector.cpp</kbd></p>
  </section>
</section>

{% include 'slides/closing.njk' %}
