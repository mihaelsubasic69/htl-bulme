write presentations around solving a problem (eg. vector)

POS3 DONE (use as input for generic course overview)
- OOP 1.1 - template classes & template methods; translation units
  https://isocpp.org/wiki/faq/templates#templates-defn-vs-decl
  https://stackoverflow.com/q/44335046/104659
- OOP 1.5 - operator overloading; use existing presentation

POS3 TODO
- OOP 1.6 copy assignment, copy constructor etc. for vector example
  https://stackoverflow.com/questions/121162/what-does-the-explicit-keyword-mean
  rule of the big 3 / 4 / 4 1/2 "if you provide your own version of any one
    them, you should provide your own version of all."
  in the future, do this before overloading
  big 3; removing the default implementations
  https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#c20-if-you-can-avoid-defining-default-operations-do
  https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#c30-define-a-destructor-if-a-class-needs-an-explicit-action-at-object-destruction
  https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#c21-if-you-define-or-delete-any-copy-move-or-destructor-function-define-or-delete-them-all
  https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#c31-all-resources-acquired-by-a-class-must-be-released-by-the-classs-destructor
  friend declaration of operator<< to be able to iterate over elements

- OOP 2 - composition and aggregation; Resource acquisition is initialization
  https://en.wikipedia.org/wiki/Resource_acquisition_is_initialization
  use terms of https://www.senarclens.eu/~gerald/presentations/cms1/7_basic_classes/#(3)
  return [*]this from member methods (for method chaining etc); best practices?
  `explicit` constructor

- OOP 2.5 - unit testing C++ classes (gtest or catch2, friend keyword etc)

- OOP 3 - inheritance and polymorphism; protected access
https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#c10-prefer-concrete-types-over-class-hierarchies
public and private inheritance (+ the guidelines' recommendations)

template specialization

- CPP smart pointer etc.

- OOP 4 - exceptions and exception handling

leave out for now
- OOP 5 - move constructors, rule of 4 / 5

--- above until XMAS

GUI (overview of free cross platform GUI toolkits)
-> wxwidgets
- qt
- gtkmm
  https://gitlab.gnome.org/GNOME/gtkmm-documentation/-/tree/master/examples
  `g++ *.cc -o program `pkg-config gtkmm-4.0 --cflags --libs``

GUI - show how to create a wxwidgets GUI
task: add features (like points etc) to wxwidgets tetris example

--- above until end of Jan

Functional Programming
-> basics (recursion, anonymous functions, ...)
- advanced (currying, ...)

--- above before end of term; maybe do recursion with common C examples in
Thursday classes after XMAS

Tools and conventions
- coding styles + linters + including them in a CI; editorconfig etc.
- profiler / performance tuning
- finding memory leaks via kcachegrind, valgrind etc.

C++ Algorithms (make deck generic to be used with different programming
languages)
explain
- stack (explain with array implementation)
- queue (explain with both array and doubly linked list)
- dequeue (explain as doubly linked list with already created svg)
- priority queue

with runtime and memory implications and small real world examples
