#include <time.h>
#include <stdlib.h>
#include <stdio.h>

void read_values(int values[], size_t count) {
  srand(time(NULL));  // Initialize random number generator
  for (size_t i = 0; i < count; ++i) {
    values[i] = rand();  // assume the values are read from user input
  }
}

int main() {
  int values[3];  // number of elements known at compile time
  read_values(values, 3);
  printf("{ %d, %d, %d }\n", values[0], values[1], values[2]);
  return 0;
}
