#include <stdio.h>

void print_info(int values[], size_t count) {  // values is actually a pointer
  // sizeof returns the size of the pointer (64 bits on modern systems)
  printf("sizeof(values): %ld\n", sizeof(values));
  printf("sizeof(values) / sizeof(int): %ld\n", sizeof(values) / sizeof(int));
  // only way to know the number of elements is a separate argument
  printf("number of elements: %ld\n", count);
}

int main() {
  int values[] = {-5, 39, 13, -21};  // compiler determines number of elements
  printf("sizeof(values): %ld\n", sizeof(values));
  printf("sizeof(values) / sizeof(int): %ld\n", sizeof(values) / sizeof(int));
  print_info(values, sizeof(values) / sizeof(int));
  return 0;
}
