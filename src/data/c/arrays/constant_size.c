#include <stdio.h>

int main() {
  unsigned int numbers[3];  // number of elements known at compile time
  numbers[0] = 15;
  numbers[2] = 5;
  numbers[1] = 10;
  printf("{ %d, %d, %d }", numbers[0], numbers[1], numbers[2]);
  return 0;
}
