#include <stdio.h>

int main() {
  int values[] = {-5, 39, 13, -21};  // compiler determines number of elements
  printf("used memory: %ld\n", sizeof(values));
  printf("number of elements: %ld\n", sizeof(values) / sizeof(int));
  return 0;
}
