#include <stdio.h>
typedef struct data {
  int first;
  int second;
} Data;
int main() {
  Data d;
  d.first = 1724;
  d.second = 255;
  FILE* f = fopen("data.bin","wb");
  if (f == NULL) { fputs("Error!", stderr); return 1; }
  fwrite(&d, sizeof(Data), 1, f);
  fclose(f);
  return 0;
}
