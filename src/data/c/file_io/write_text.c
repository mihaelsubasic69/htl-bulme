#include <stdio.h>  // provides fopen, fprintf and fclose

int main() {
  int num = 15;
  FILE* f = fopen("data.txt", "w");  // open for writing

  if(f == NULL) {  // we have to check if opening was successful
    fprintf(stderr, "Error!");
    return 1;
  }

  fprintf(f, "%d\n", num);
  fprintf(f, "This is a text\n");
  fclose(f);

  return 0;
}
