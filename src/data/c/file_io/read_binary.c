#include <stdio.h>
typedef struct data {
  int first;
  int second;
} Data;
int main() {
  Data d;
  FILE* f = fopen("data.bin","rb");
  if (f == NULL) { fputs("Error!", stderr); return 1; }
  fread(&d, sizeof(d), 1, f);
  fclose(f);
  printf("first: %d, second: %d\n", d.first, d.second);
  return 0;
}
