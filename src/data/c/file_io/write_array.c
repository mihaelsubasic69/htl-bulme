#include <stdio.h>
#include <limits.h>

int main() {
  unsigned int arr[] = { 0, UINT_MAX, 255, 0xff000000, 1724 };
  FILE* f = fopen("array.bin","wb");
  if (f == NULL) { fputs("Error!", stderr); return 1; }
  fwrite(arr, sizeof(unsigned int), sizeof(arr) / sizeof(unsigned int), f);
  fclose(f);
  return 0;
}
