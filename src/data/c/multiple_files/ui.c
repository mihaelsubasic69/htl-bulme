#include "ui.h"  // local includes use quotation marks

#include <stdio.h>
#include <stdlib.h>

#include "array.h"

size_t get_dimension(int argc, char** argv) {
  size_t dimension = 0;
  if (argc == 1) {
    printf("Error: no arguments given.\n");
    printf("Usage: `array_tool dimension`\n");
    printf("    dimension is an integer >= 1\n");
    exit(1);
  } else if (argc == 2) {
    sscanf(argv[1], "%lu", &dimension);
  }
  return dimension;
}

void print_array(int array[], size_t dimension) {
  printf("[");
  for (size_t i = 0; i < dimension - 1; ++i) {
    printf("%d, ", array[i]);
  }
  printf("%d]\n", array[dimension - 1]);
}

void print_shared() {
  printf("shared: %d\n", shared);  // shared is declared in array.h
}
