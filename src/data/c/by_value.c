#include <stdio.h>

void pass_by_value(int i) {
  i = 5;
  printf("i: %d (at the end of the called function)\n", i);
}

int main() {
   int i = 3;
   printf("i: %d\n", i);
   pass_by_value(i);
   printf("i: %d (after returning from function)\n", i);
   return 0;
}
