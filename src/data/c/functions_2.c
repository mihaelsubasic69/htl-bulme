
/*********************************************************************
 * Exercises for practicing C functions.
 *
 * To compile using math.h, it's necessary to add '-lm' when
 * compiling.
 *
 * \author Gerald Senarclens de Grancy
 *
 * \return 0, if all tests passed, otherwise 1
*********************************************************************/

#include <stdbool.h>
#include <stdio.h>
#include <math.h>

int main();
bool test_determineGrade();
bool test_vectorDistance();
bool test_weeklySalary();
int determineGrade(double percentage_result);
double vectorDistance(double x_a, double y_a, double x_b, double y_b);
double weeklySalary(double hours, double hourly_salary);

/**
 * Run all test functions.
 *
 * Stop as soon as an error occurs.
 *
 * \return 0, if all tests passed, otherwise 1.
 */
int main()
{
  if (!test_determineGrade()) return 1;
  if (!test_vectorDistance()) return 1;
  if (!test_weeklySalary()) return 1;
  return (0);
}

/**
 * Return the grade corresponding to the given percentage value.
 *
 * \param[in] percentage_result Between 0.0 and 100.0.
 *
 * \return The determined grade.
*/
int determineGrade(double percentage_result)
{

}

/**
 * Test cases:
 * {0.0, 15.5, 50.0} -> 5
 * {50.1, 55.0, 65.0} -> 4
 * {65.5, 70.5, 80.0} -> 3
 * {81.0, 85.1, 90.0} -> 2
 * {90.5, 95.3, 100.0} -> 1
 */
bool test_determineGrade()
{
  double five[] = {0.0, 15.5, 50.0};
  double four[] = {50.1, 55.0, 65.0};
  double three[] = {65.5, 70.5, 80.0};
  double two[] = {81.0, 85.1, 90.0};
  double one[] = {90.5, 95.3, 100.0};
  for (int i=0; i<3; i++) {
    if (determineGrade(five[i]) != 5) {
      printf("%lf%% should be graded with 5, but got %d\n",
             five[i], determineGrade(five[i]));
      return false;
    }
    if (determineGrade(four[i]) != 4) {
      printf("%lf%% should be graded with 4, but got %d\n",
             four[i], determineGrade(four[i]));
      return false;
    }
    if (determineGrade(three[i]) != 3) {
      printf("%lf%% should be graded with 3, but got %d\n",
             three[i], determineGrade(three[i]));
      return false;
    }
    if (determineGrade(two[i]) != 2) {
      printf("%lf%% should be graded with 2, but got %d\n",
             two[i], determineGrade(two[i]));
      return false;
    }
    if (determineGrade(one[i]) != 1) {
      printf("%lf%% should be graded with 1, but got %d\n",
             one[i], determineGrade(one[i]));
      return false;
    }
  }
  return true;
}

/**
 * Test cases
 *   x_a |   y_a |   x_b |   y_b | distance
 *   5.0 |   5.0 |   5.0 |   5.0 |      0.0
 *   0.0 |   0.0 |   4.0 |   3.0 |      5.0
 *  -2.0 |  -2.0 |   2.0 |  -5.0 |      5.0
 *   1.3 |  -4.1 |  -7.3 |   2.9 | 11.08873
 */
bool test_vectorDistance()
{
  double granularity = 0.001;
  double x_a_values[] = {5.0, 0.0, -2.0,  1.3};
  double y_a_values[] = {5.0, 0.0, -2.0, -4.1};
  double x_b_values[] = {5.0, 4.0,  2.0, -7.3};
  double y_b_values[] = {5.0, 3.0, -5.0,  2.9};
  double expected_values[] = {0.0, 5.0, 5.0, 11.08873};
  for (int i=0; i<4; ++i) {
    double actual = vectorDistance(x_a_values[i], y_a_values[i],
                                   x_b_values[i], y_b_values[i]);
    if (fabs(actual - expected_values[i]) > granularity) {
      printf("The distance from %lf/%lf to %lf/%lf should be %lf but is %lf.\n",
             x_a_values[i], y_a_values[i],
             x_b_values[i], y_b_values[i],
             expected_values[i], actual);
      return false;
    }
  }
  return true;
}

/**
 * Test cases:
 * hours | salary | result
 *   0.0 | 1000.0 |    0.0
 *  60.0 |    0.0 |    0.0
 *  20.0 |   25.0 |  500.0
 *  40.0 |   25.0 | 1000.0
 *  50.0 |   20.0 | 1100.0
 */
bool test_weeklySalary()
{
  double granularity = 0.001;
  double hours[] = {0.0, 60.0, 20.0, 40.0, 50.0};
  double salaries[] = {1000.0, 0.0, 25.0, 25.0, 20.0};
  double expected_values[] = {0.0, 0.0, 500.0, 1000.0, 1100.0};
  for (int i=0; i<5; i++) {
    double actual = weeklySalary(hours[i], salaries[i]);
    if (fabs(actual - expected_values[i]) > granularity) {
      printf("%lf hours at %lf Euros should be %lf Euros but is %lf\n",
             hours[i], salaries[i], expected_values[i], actual);
      return false;
    }
  }
  return true;
}

