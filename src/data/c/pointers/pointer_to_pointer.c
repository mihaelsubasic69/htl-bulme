#include <stdio.h>

int main() {
  int a = 4, b = 5;
  int* a_ptr = &a;  // when starting, it is easiest to have a pre- or suffix
  int* b_ptr = &b;  // for every pointer in your programs
  int** p = &a_ptr;
  *p = NULL;  // set a_ptr to invalid location
  *p = b_ptr;
  **p *= **p;  // use multiple lines to make readable
  printf("a = %d, b = %d\n", a, b);
  return 0;
}
