#include <stdio.h>

int sum(const int* array, int count) {
  int value = 0;
  while (count--) {
    value += *array++;
  }
  return value;
}

int main() {
  int array[] = {1, 2, 3, 4, 5, 6};
  int value = sum(array, sizeof(array) / sizeof(int));
  printf("sum: %d\n", value);
  return 0;
}
