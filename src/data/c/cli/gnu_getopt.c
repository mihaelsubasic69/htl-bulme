#include <stdio.h>
#include <stdbool.h>
#include <getopt.h>  // getopt_long
int main (int argc, char **argv) {
  int c;
  int has_a = false, has_b = false;  // flags
  int v = 0;
  char *c_arg = 0, *d_arg = 0;  // option_arguments
  static struct option long_options[] = {
  /*   NAME       ARGUMENT           FLAG  SHORTNAME */
    {"add",     required_argument, NULL, 0},
    {"append",  no_argument,       NULL, 1000},
    {"delete",  required_argument, NULL, 0},
    {"verbose", no_argument,       NULL, 'v'},
    {"create",  required_argument, NULL, 'c'},
    {"file",    required_argument, NULL, 0},
    {NULL,      0,                 NULL, 0}
  };
  int option_index = 0;
  while ((c = getopt_long(argc, argv, "abc:d:v",
          long_options, &option_index)) != -1) {
    switch (c) {
    case 0:
      printf ("option %s", long_options[option_index].name);
      if (optarg) {
        printf (" with arg %s", optarg);
      }
      printf ("\n");
      break;
    case 1000:  // append
      puts("`append` option was given");
      break;
    case 'a':
      has_a = true;
      break;
    case 'b':
      has_b = true;
      break;
    case 'c':
      c_arg = optarg;
      break;
    case 'd':
      d_arg = optarg;
      break;
    case 'v':
      ++v;
    case '?':
      break;
    default:
      printf ("?? getopt returned character code 0%o ??\n", c);
    }
  }
  if (optind < argc) {
    printf ("non-option ARGV-elements: ");
    while (optind < argc) {
      printf ("%s ", argv[optind++]);
    }
    printf ("\n");
  }
  printf ("a: %s, b: %s, c_arg: %s, d_arg: %s, verbosity-level: %d\n",
          has_a ? "true" : "false", has_b ? "true" : "false", c_arg, d_arg, v);
  return 0;
}
