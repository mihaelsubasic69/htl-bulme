#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

int case_insensitive_compare(const void* a, const void* b) {
  return strcasecmp(*(const char**)a, *(const char**)b);
}

int main(void) {
  const char* names[] = {"Harry", "Anne", "Mary", "chris", "Pat", "eric", "Lily"};
  qsort(names, 7, sizeof(const char*), case_insensitive_compare);
  puts("names (sorted ascending)");
  for (int i = 0; i < 7; ++i) {
    puts(names[i]);
  }
  return 0;
}
