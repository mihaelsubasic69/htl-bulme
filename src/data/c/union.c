#include <stdio.h>

union U {
  int i, j;
 };

int main(void) {
  union U u;
  u.i = 5;
  printf("u.i: %d, u.j: %d\n", u.i, u.j);
  u.j = 25;
  printf("u.i: %d, u.j: %d\n", u.i, u.j);
  return 0;
}
