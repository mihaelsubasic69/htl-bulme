#include <stdio.h>

typedef struct {
  float x;
  float y;
} Point;

Point centroid(Point A, Point B, Point C) {
    float x_G = 1.0 / 3.0 * (A.x + B.x + C.x);
    float y_G = 1.0 / 3.0 * (A.y + B.y + C.y);
    Point G = {x_G, y_G};
    return G;
}

int main() {
  Point A = {1.5, 3.0}, B = {12.0, 3.0}, C = {4.5, 9.0};
  Point G = centroid(A, B, C);  // pass by value
  return 0;
}
