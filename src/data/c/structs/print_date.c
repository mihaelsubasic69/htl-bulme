#include <stdio.h>

typedef struct {
  int year;
  int month;
  int day;
} Date;

void print_date(Date d) {
  printf("%04d-%02d-%02d\n", d.year, d.month, d.day);
}

int main() {
  Date today = {2021, 12, 9};
  print_date(today);
  return 0;
}
