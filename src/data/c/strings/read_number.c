#include <stdio.h>

int main() {
  char line[5];  // max. 4 characters + \0
  int number = 0;
  printf("enter an integer (0 <= number <= 9999): ");
  fgets(line, sizeof(line), stdin);
  printf("You entered the text `%s`.\n", line);
  sscanf(line, "%d", &number);
  printf("This text, represented as an integer is `%d`.\n", number);
}
