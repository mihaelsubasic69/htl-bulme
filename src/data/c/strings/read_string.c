#include <stdio.h>
#include <string.h>

int main() {
  char line[15];  // max. 14 characters + \0
  printf("enter your first name: ");
  fgets(line, 15, stdin);
  printf("{begin}%s{end}\n", line);
  line[strcspn(line, "\n")] = 0;  // remove final newline; '\0' == 0
  printf("{begin}%s{end}\n", line);
  return 0;
}
