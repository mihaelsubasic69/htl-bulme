def add_to(n):
    """
    Return the sum of all values from 1 to n.
    """
    sum = 0
    for i in range(n+1):
        sum += i
    return sum
