from before_refactoring import add_to
# from after_refactoring import add_to

def test_add_to():
    assert add_to(0) == 0
    assert add_to(25) == 325
    assert add_to(26) == 351
    assert add_to(500000000) == 125000000250000000

def test_add_to_slow():
    assert add_to(4294967295) == 9223372034707292160
