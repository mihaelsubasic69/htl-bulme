#!/usr/bin/env python3
"""
Small sample implementation showing how to solve a mixed integer program.

The MILP example for this demo is the transportation problem with sortation
centers.

This implementation demonstrates the PuLP modeling packages and uses the
default open source solver CBC.  It tries to be proper by having each
constraint set reside in a separate function.  The input is read from a JSON
file and represented using a simple object structure.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import pulp
from pulp import lpSum

import argparse
import json
import sys

from collections import namedtuple
from datetime import datetime


class OptimizationObject(object):
    def __str__(self):
        return self.name

    def __repr__(self):
        return str(self)

    def __lt__(self, other):
        return self.name < other.name


class Customer(OptimizationObject):
    def __init__(self, c: dict):
        self.name = c['name']
        self.demand = c['demand']


class FulfillmentCenter(OptimizationObject):
    def __init__(self, fc: dict):
        self.name = fc['name']
        self.supply = fc['supply']


class SortationCenter(OptimizationObject):
    def __init__(self, sortation_center: dict, fulfillment_centers: dict,
                 customers: dict):
        self.name = sortation_center['name']
        self.unit_costs = {fulfillment_centers[fc['from']]: fc['cost']
                           for fc in sortation_center['unit_cost']}
        self.shipping_costs = {customers[cust['to']]: cust['cost']
                               for cust in sortation_center['shipping_cost']}


class Problem(object):
    def __init__(self, filename):
        self.instance = filename
        with open(filename, encoding='utf-8') as f:
            json_pb = json.load(f)
        self.__customers = {}
        for c_dict in json_pb['customers']:
            self.__customers[c_dict['name']] = Customer(c_dict)
        self.__fulfillment_centers = {}
        for fc_dict in json_pb['fulfillment_centers']:
            self.__fulfillment_centers[fc_dict['name']] = FulfillmentCenter(
                fc_dict)
        self.__sortation_centers = {}
        for sc_dict in json_pb['sortation_centers']:
            self.__sortation_centers[sc_dict['name']] = SortationCenter(
                sc_dict, self.__fulfillment_centers, self.__customers)

    def customer(self, name):
        return self.__customers[name]

    @property
    def customers(self):
        return self.__customers.values()

    def fulfillment_center(self, name):
        return self.__fulfillment_centers[name]

    @property
    def fulfillment_centers(self):
        return self.__fulfillment_centers.values()

    def sortation_center(self, name):
        return self.__sortation_centers[name]

    @property
    def sortation_centers(self):
        return self.__sortation_centers.values()

    def __str__(self):
        return ('''Transportation Problem with Sortation Centers ({})
{} customers
{} fulfillment_centers
{} sortation_centers'''.format(self.instance, len(self.customers),
                               len(self.fulfillment_centers),
                               len(self.sortation_centers)))


def __objective_function(index, p, milp, x_ns, y_sm, **kwargs):
    """The objective function."""
    milp += (lpSum(x_ns[n][s] * s.unit_costs[n] for n in p.fulfillment_centers
                   for s in p.sortation_centers) +
             lpSum(y_sm[s][m] * s.shipping_costs[m]
                   for s in p.sortation_centers for m in p.customers),
             '({}) objective function'.format(index))


def __constraint_fulfill_deliveries(index, p, milp, y_sm, **kwargs):
    """Ensure that all customer orders are fulfilled with a single delivery."""
    for m in p.customers:
        milp += (lpSum(y_sm[s][m] for s in p.sortation_centers) == 1,
                 '({}) {}'.format(index, m))


def __constraint_goods_sortation_centers(index, p, milp, x_ns, y_sm, **kwargs):
    """Ship correct amount of goods to each sortation center"""
    for s in p.sortation_centers:
        milp += (lpSum(x_ns[n][s] for n in p.fulfillment_centers) ==
                 lpSum(y_sm[s][m] * m.demand for m in p.customers),
                 '({}) {}'.format(index, s))


def __constraint_supply_fulfillment_centers(index, p, milp, x_ns, **kwargs):
    """Ensure that the fulfillment centers do not run out of goods."""
    for n in p.fulfillment_centers:
        milp += (lpSum(x_ns[n][s] for s in p.sortation_centers) <= n.supply,
                 '({}) {}'.format(index, n))


__model = (
    __objective_function,
    __constraint_fulfill_deliveries,
    __constraint_goods_sortation_centers,
    __constraint_supply_fulfillment_centers
)


def model_doc():
    '''Return a high-level textual representation of the model.'''
    text = ''

    def header(doc):
        '''Return the first non-empty line of the doc.'''
        lines = doc.split('\n')
        for line in lines:
            if line.strip():
                return line

    for index, equations in enumerate(__model, 1):
        text += '({:>2}) {}\n'.format(index, header(equations.__doc__))
    return text


def model(p: Problem):
    """
    Return the MILP model for the

    TODO: split up into separate functions
    """
    x_ns = pulp.LpVariable.dicts('x_%s_%s',
                                 (p.fulfillment_centers, p.sortation_centers),
                                 cat=pulp.LpInteger, lowBound=0)
    y_sm = pulp.LpVariable.dicts('y_%s_%s',
                                 (p.sortation_centers, p.customers),
                                 cat=pulp.LpBinary)
    # declare problem
    milp = pulp.LpProblem('Transportation Problem with Sortation Centers',
                          pulp.LpMinimize)
    for index, equations in enumerate(__model, 1):
        equations(**locals())
    return milp


class Solution(object):
    def __init__(self, p: Problem, milp: pulp.LpProblem):
        """
        Using strings instead of objects to represent DCtoSC and SCtoDC keys
        risks nasty bugs if objects may have the same name. However, for
        the purpose of playing around w/ small examples the increased usability
        of being able to use strings as dict keys is hopefully worth it.
        """
        self.instance = p.instance
        assert (milp.status == pulp.LpStatusNotSolved or
                milp.status == pulp.LpStatusOptimal), "problem is {}".format(
            pulp.LpStatus[milp.status])
        self.value = milp.objective.value()
        self.DCtoSC = {fc: {sc: 0 for sc in p.sortation_centers}
                       for fc in p.fulfillment_centers}
        self.SCtoCust = {sc: {c: 0 for c in p.customers}
                         for sc in p.sortation_centers}
        for var in milp.variables():
            if var.name.startswith('x'):
                fc = p.fulfillment_center(var.name.split('_')[1])
                sc = p.sortation_center(var.name.split('_')[-1])
                self.DCtoSC[fc][sc] = var.value()
            elif var.name.startswith('y'):
                sc = p.sortation_center(var.name.split('_')[1])
                c = p.customer(var.name.split('_')[-1])
                self.SCtoCust[sc][c] = var.value() * c.demand

    def __str__(self):
        dc_to_sc = ''
        for key in sorted(self.DCtoSC):
            dc_to_sc += '{}: {}\n'.format(key,
                                          {k: v for k, v in
                                           self.DCtoSC[key].items() if v})
        sc_to_cust = ''
        for key in sorted(self.SCtoCust):
            sc_to_cust += '{}: {}\n'.format(key,
                                            {k: v for k, v in
                                             self.SCtoCust[key].items() if v})
        return 'value: {}\n\nDC to SC\n{}\nSC to Cust\n{}'.format(self.value,
                                                                  dc_to_sc,
                                                                  sc_to_cust)

    def to_json(self):
        def sanitized(d: dict):
            return {str(k): {str(ik): iv for ik, iv in v.items()}
                    for k, v in d.items()}
        sol = {'value': self.value,
               'DCtoSC': sanitized(self.DCtoSC),
               'SCtoCust': sanitized(self.SCtoCust)}
        return json.dumps(sol, indent=2)


def main(args=None):
    """
    Read one or more problem files, solve them and print their solution stats.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--full-model', action='store_true',
                        help='only print the complete model')
    parser.add_argument('--json', action='store_true',
                        help='output json formatted string')
    parser.add_argument('--lp', action='store_true',
                        help='output the lp model to file')
    parser.add_argument('-m', '--model', action='store_true',
                        help='only print the high-level model')
    parser.add_argument('filenames', nargs='+',
                        help='one or more input files')

    args = parser.parse_args(args)
    for filename in args.filenames:
        start = datetime.now()
        print('start:', start)
        p = Problem(filename)
        print(p)
        print()
        milp = model(p)
        if args.full_model:
            print(milp)
            continue
        if args.lp:
            milp.writeLP(filename + '.lp')
            continue
        if args.model:
            print(model_doc())
            continue
        milp.solve()
        sol = Solution(p, milp)
        if args.json:
            print(sol.to_json(), end='\n'*2)
        else:
            print(sol)
        end = datetime.now()
        print('end: {}, duration: {}'.format(end, end - start))


if __name__ == '__main__':
    sys.exit(main())
