#include <iostream>
// templates may also have non-type arguments
// an example is std::array which is just a wrapper around fixed size arrays
template<class T, size_t N>
struct array {
  size_t size = N;
  T data[N];
};

int main() {
  array<int, 3> a;
  a.data[0] = 1, a.data[1] = 2, a.data[2] = 3;
  std::cout << "a: {" << a.data[0] << ", " << a.data[1] << ", " << a.data[2];
  std::cout << "}" << std::endl;
  return 0;
}
