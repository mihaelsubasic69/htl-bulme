#include <iostream>
#include "2_function_template.hpp"
int main() {
    std::cout << max(5, 6) << std::endl;  // calls max(int, int)
    std::cout << max(1.2, 3.4) << std::endl;  // calls max(double, double)
    return 0;
}
