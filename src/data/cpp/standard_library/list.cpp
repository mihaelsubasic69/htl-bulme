#include <algorithm>
#include <iostream>
#include <list>
int main() {
    std::list<int> l{7, 5, 16, 8};
    l.push_front(1);  // add to the front: O(1)
    l.push_back(3);  // add to the back: O(1)
    // Find iterator pointing to 8
    auto it = std::find(l.begin(), l.end(), 8);
    if (it != l.end())  // if found
        l.insert(it, 42);  // insert `42` before element `8`
    // Print out the list
    std::cout << "{";
    for (int n : l)
        std::cout << n << ", ";
    std::cout << "}" << std::endl;
}
