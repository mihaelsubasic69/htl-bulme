#include <iostream>
#include <map>
#include <string>

void print_map(const std::map<std::string, int>& m) {
  std::cout << "{";
  for (const auto& [key, value] : m)  // C++17
    std::cout << "\"" << key << "\": " << value << ", ";
//  for (const auto& e : m)  // C++ 11
//      std::cout << e.first << ": " << e.second << "; ";
  std::cout << "}" << std::endl;
}

int main() {
  std::map<std::string, int> m{ {"Henri", 3}, {"Leonie", 6}, {"Marie", 9} };
  print_map(m);
  m["Henri"] = 4;  // update an existing value
  m["Sue"] = 10;  // insert a new value
  print_map(m);
  // using operator[] with non-existent key always performs an insert
  std::cout << m["Louisa"] << std::endl;
  print_map(m);
  m.erase("Louisa");  // remove element
  print_map(m);
  std::cout << "m.size(): " << m.size() << std::endl;
  m.clear();
  std::cout << std::boolalpha << "Is map empty? " << m.empty() << std::endl;
}
