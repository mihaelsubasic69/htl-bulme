#include <iterator>
#include <iostream>
#include <algorithm>
#include <array>
int main() {
  std::array<int, 5> a1{4, 2, 3, 1, 5};
  std::array a2{1.5, 2.0, 3.1};  // since C++17: clang++ -std=c++17
  // container operations are supported
  std::cout << "a1 has " << a1.size() << " elements" << std::endl;
  std::sort(a1.begin(), a1.end());
  std::copy(a1.begin(), a1.end(),
            std::ostream_iterator<int>(std::cout, " "));
  std::cout << std::endl;
  // range-based for loop is supported
  for(const auto& elem: a2)
    std::cout << elem << ' ';
  std::cout << std::endl;
}
