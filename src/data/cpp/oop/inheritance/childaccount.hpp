// SPDX-FileCopyrightText: 2023 Gerald Senarclens de Grancy <oss@senarclens.eu>
// SPDX-License-Identifier: MIT

#ifndef CHILDACCOUNT_H
#define CHILDACCOUNT_H

#include "account.hpp"

namespace bank {


class ChildAccount : public Account {
public:
  ChildAccount(std::string owner) : Account(owner) {}
  ChildAccount(std::string owner, unsigned int deposit)
    : Account(owner, deposit) {}
  unsigned int withdraw(unsigned int amount);
};

}  // namespace bank

#endif // CHILDACCOUNT_H
