// SPDX-FileCopyrightText: 2023 Gerald Senarclens de Grancy <sg@bulme.at>
// SPDX-License-Identifier: MIT

#include "childaccount.hpp"

namespace bank {

unsigned int ChildAccount::withdraw(unsigned int amount) {
  if (amount > balance()) {
    amount = balance();
  }
  return Account::withdraw(amount);
}

}  // namespace bank
