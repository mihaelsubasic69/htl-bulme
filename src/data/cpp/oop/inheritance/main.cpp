#include <iostream>

#include "account.hpp"
#include "childaccount.hpp"

using bank::Account;
using bank::ChildAccount;

int main() {
  Account a {"Gerald"};
  std::cout << a << std::endl;
  a.deposit(2000);
  std::cout << a << std::endl;
  a.withdraw(2500);
  std::cout << a << std::endl;

  Account a2 {"Timy"};
  std::cout << a2 << std::endl;
  a2.deposit(20000);
  std::cout << a2 << std::endl;
  a2.withdraw(5000);
  std::cout << a2 << std::endl;

  ChildAccount ca {"Eric"};
  ca.deposit(5000);
  std::cout << ca << std::endl;
  std::cout << "withdrawing 10000000 leads to actually withdrawing " <<
    ca.withdraw(10000000) << std::endl;
  std::cout << ca << std::endl;

  ChildAccount ca2 {"Mary"};
  Account* ap = &ca2;
  std::cout << *ap << std::endl;
  ap->deposit(2000);
  std::cout << *ap << std::endl;
  ap->withdraw(2500);
  std::cout << *ap << std::endl;
}
