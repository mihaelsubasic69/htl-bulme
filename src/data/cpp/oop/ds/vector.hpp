#ifndef VECTOR_HPP
#define VECTOR_HPP
#include <iostream>
namespace ds {  // namespace for our own data structures (ds)
// very simplified vector class
template <class T>
class Vector {
public:
  Vector() : size_{0}, space_{0}, elements_(nullptr) {};  // constructor
  ~Vector() { delete[] elements_; }  // destructor
  void push_back(T elem);
  T pop_back();
  std::size_t size() { return size_; }  // optimized by compiler if in header
private:
  std::size_t size_;
  std::size_t space_;
  T* elements_;
  void resize(std::size_t size);
};

template<class T> void Vector<T>::resize(std::size_t new_space) {
  if (new_space <= space_) return;  // never shrink
  T* new_elements = new T[new_space];
  for (std::size_t i(0); i < size_; ++i) {
    new_elements[i] = elements_[i];
  }
  delete[] elements_;
  elements_ = new_elements;
  space_ = new_space;
}

template<class T> void Vector<T>::push_back(T value) {
  if (space_ == 0) {
    resize(8);  // default size is 8
  } else if (space_ == size_) {
    resize(space_ * 2);
  }
  elements_[size_] = value;
  ++size_;
}

template<class T> T Vector<T>::pop_back() {
  if (!size_) {
    std::cerr << "ERROR: cannot pop element of empty Vector" << std::endl;
    return 0;
  }
  --size_;
  return elements_[size_];
}
}
#endif
