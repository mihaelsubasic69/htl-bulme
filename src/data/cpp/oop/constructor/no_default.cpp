#include <iostream>
// does not compile because compiler only auto-generates default constructor
// if no other constructor is defined
class Example {
public:
  Example(int a) : a_{a} {};
  int a() const { return a_; }
private:
  int a_ = 42;
};

int main() {
  Example e;  // cannot be created since there is no default constructor
  std::cout << e.a() << std::endl;
  Example e2(1);
  std::cout << e2.a() << std::endl;
}
