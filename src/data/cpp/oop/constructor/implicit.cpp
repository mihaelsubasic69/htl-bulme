#include <iostream>

class Example {
public:
  Example(int a) : a_{a} {};
  int a() { return a_; }
private:
  int a_ = 42;
};

void print_example(Example e) {
  std::cout << e.a() << std::endl;
}

int main() {
  Example e = 5;  // integer is converted to `Example` type implicitly
  std::cout << e.a() << std::endl;
  print_example(33);  // integer is converted to `Example` type implicitly
}
