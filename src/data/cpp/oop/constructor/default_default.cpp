#include <iostream>

// default constructor: a ctor which can be called with no arguments
class Example {
public:
  Example() = default;  // since C++ 11
  Example(int a) : a_{a} {};
  int a() const { return a_; }

private:
  int a_ = 42;
};

int main() {
  Example e;
  std::cout << e.a() << std::endl;
  Example e2{1};
  std::cout << e2.a() << std::endl;
}
