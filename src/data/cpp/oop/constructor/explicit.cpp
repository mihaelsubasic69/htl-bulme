#include <iostream>

class Example {
public:
  explicit Example(int a) : a_{a} {};  // do not allow implicit conversion
  int a() { return a_; }
private:
  int a_ = 42;
};

void print_example(Example e) {
  std::cout << e.a() << std::endl;
}

int main() {
  Example e = 5;  // compiler error
  std::cout << e.a() << std::endl;
  print_example(33);  // compiler error
}
