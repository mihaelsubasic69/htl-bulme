#include <iostream>

#include "point.h"

namespace draw {

class Circle {
public:
  Circle(Point* center, double radius) : radius_(radius), center_(center) {};
  void draw() { std::cout << "drawing Circle(" << center_->x() << "/"
    << center_->y() << ", " << radius_ << ")" << std::endl; }
  void center(Point* c) { center_ = c; }  // only works with pointers
private:
  double radius_ = 1.0;
  Point* center_;  // pointer can be reassigned
};

}  // namespace draw

int main(void) {
  // developer has to ensure the point stays in scope
  draw::Point center1{17, -2};
  draw::Point center2{-18, 12};
  draw::Circle c(&center1, 5);
  c.draw();
  c.center(&center2);
  c.draw();
}
