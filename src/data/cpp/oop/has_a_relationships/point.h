#ifndef POINT_HPP
#define POINT_HPP

namespace draw {

class Point {
public:
  Point() = default;
  Point(double x, double y) : x_(x), y_(y) {};
  double x() { return x_; }
  double y() { return y_; }
private:
  double x_ = 0.0;
  double y_ = 0.0;
};

} // namespace draw

#endif
