#include <iostream>

struct Point {
  int x;
  int y;
  Point(int x, int y) : x(x), y(y) {}  // constructor with initializer list
  Point() : Point(0, 0) {}  // Point() delegates to Point(0, 0)
  void print() {
    std::cout << "x: " << x << ", y: " << y << std::endl;
  }
};
int main() {
  Point* p = new Point(5, 3);  // create object on the free store
  p->print();
  delete p;  // free the reserved memory
  return 0;
}
