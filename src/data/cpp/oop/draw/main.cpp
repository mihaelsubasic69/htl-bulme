#include "point.hpp"

int main(void) {
  draw::Point p1(3, 4);  // create instance of Point from namespace draw
  draw::Point p2(5, 0);  // create another object (instance)
  p1.print();
  p2.print();
}
