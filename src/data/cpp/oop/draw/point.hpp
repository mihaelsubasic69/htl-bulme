#ifndef POINT_HPP
#define POINT_HPP

namespace draw {
struct Point {
  int x;
  int y;
  Point(int x, int y);  // constructor is declared, not defined
  Point();
  void print();  // method is declared, not defined
};
}

#endif
