#include <iostream>
#include <string>
#include <vector>

int main() {
  std::vector<std::string> names = {"Pat", "Chris", "Sue", "Steve", "Anne"};
  for (std::vector<std::string>::size_type i(0); i < names.size(); ++i) {
    std::cout << names[i] << std::endl;
  }
  std::cout << std::endl;
  return 0;
}
