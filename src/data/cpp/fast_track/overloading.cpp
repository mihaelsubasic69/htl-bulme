#include <iostream>
void print_array(int array[], size_t count);  // function declaration
void print_array(double* array, size_t count);  // overload function

int main(void) {
  int a1[] {1, 2, 3, 4};
  double a2[] {3.7, 9.4, 1.1, 12.9, -0.3};
  print_array(a1, 4);
  print_array(a2, 5);
  return 0;
}
void print_array(int array[], size_t count) {
  for (size_t i = 0; i < count; ++i) {
    std::cout << array[i] << std::endl;
  }
}
void print_array(double* array, size_t count) {
  while (count) {
    std::cout << *array << std::endl;
    ++array;
    --count;
  }
}
