#include <iostream>

int main() {
  bool b1(true);
  auto b2 = not b1;  // `bool b2` deducted from assignment
  char c1(65);
  decltype(c1) c2;  // `char c2` deducted without assignment
  c2 = 66;
  std::cout << b1 << " " << b2 << std::endl;
  std::cout << c1 << " " << c2 << std::endl;
  return 0;
}
