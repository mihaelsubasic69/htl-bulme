#include <iostream>

/* Hello World program written in C++. */
int main(void) {
  std::cout << "Hello, World!" << std::endl;  // output some text
  return 0;  // 0 means success
}
