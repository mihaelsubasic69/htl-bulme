#include <iostream>
int main(int argc, char* argv[]) {
  switch (argc) {  // switch evaluates an expression: (argc)
  case 1:  // if the result of the expression resolves to 1, jump here
    std::cout << "Only the command was entered." << std::endl;
    break;  // break - jump out of the 'switch' block to avoid falltrough
  case 2:
    std::cout << "Command plus one argument" << std::endl;
    break;
  case 3:
    std::cout << "Command plus two arguments" << std::endl;
    break;
  default:  // any other value of the expression jumps here
    std::cout << "Command plus " <<  argc-1 << " arguments" << std::endl; break;
  }
  return 0;
}
