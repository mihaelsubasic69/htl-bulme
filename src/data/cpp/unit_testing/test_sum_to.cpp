#include "gtest/gtest.h"

unsigned long long add_to(unsigned n);  // usually in header file

TEST(sum_to, fast_test) {
  EXPECT_EQ(add_to(0), 0);
  EXPECT_EQ(add_to(25), 325);
  EXPECT_EQ(add_to(26), 351);
  EXPECT_EQ(add_to(500000000), 125000000250000000);
}

// does not terminate for the old code version due to an overflow of the
// loop counter (the loop would terminate only at UINT_MAX + 1)
TEST(sum_to, slow_test) {
  EXPECT_EQ(add_to(4294967295), 9223372034707292160);
}
