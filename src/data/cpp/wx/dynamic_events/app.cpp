#include "app.hpp"

#include <wx/wx.h>

#include "dynamic_events.hpp"

bool App::OnInit() {
  MainFrame* mainFrame = new MainFrame("C++ GUI");
  mainFrame->SetClientSize(800, 600);
  mainFrame->Center();
  mainFrame->Show();
  return true;
}

wxIMPLEMENT_APP(App);
