#include "dynamic_events.hpp"

#include <iostream>
#include <string>

#include <wx/wx.h>
#include <wx/spinctrl.h>


MainFrame::MainFrame(const wxString& title)
  : wxFrame(nullptr, wxID_ANY, title) {
  wxPanel* panel = new wxPanel(this);

  wxButton* button_add = new wxButton(panel, wxID_ANY, "Bind Events",
                                      wxPoint(20, 20), wxSize(200, 50));
  wxButton* button_remove = new wxButton(panel, wxID_ANY, "Unbind Events",
                                      wxPoint(240, 20), wxSize(200, 50));
  // https://wiki.wxwidgets.org/Avoiding_Memory_Leaks#Child_windows
  slider = new wxSlider(panel, wxID_ANY, 0, 0, 100,
                        wxPoint(300, 200), wxSize(200, -1));
  text = new wxTextCtrl(panel, wxID_ANY, "", wxPoint(300, 375),
                        wxSize(200, -1));

  button_add->Bind(wxEVT_BUTTON, &MainFrame::on_button_add_clicked, this);
  button_remove->Bind(wxEVT_BUTTON, &MainFrame::on_button_remove_clicked, this);

  // dynamically remove event binding
  CreateStatusBar();
}

void MainFrame::on_button_add_clicked(wxCommandEvent& evt) {
  slider->Bind(wxEVT_SLIDER, &MainFrame::on_slide_changed, this);
  text->Bind(wxEVT_TEXT, &MainFrame::on_text_changed, this);
  wxLogStatus("Event bindings added to other controls");
}

void MainFrame::on_button_remove_clicked(wxCommandEvent& evt) {
  slider->Unbind(wxEVT_SLIDER, &MainFrame::on_slide_changed, this);
  text->Unbind(wxEVT_TEXT, &MainFrame::on_text_changed, this);
  wxLogStatus("Event bindings removed from other controls");
}

void MainFrame::on_slide_changed(wxCommandEvent& evt) {
  std::string str("Slider Value: " + std::to_string(evt.GetInt()));
  wxLogStatus(str.c_str());
}

void MainFrame::on_text_changed(wxCommandEvent& evt) {
  std::string str("Text: " + evt.GetString());
  wxLogStatus(str.c_str());
}
